FROM gradle:jdk17

# GET AUDIVERIS CODE
# RUN apk update && apk add git
RUN apt-get update && apt-get install -y git
WORKDIR /
RUN git clone https://github.com/Audiveris/audiveris.git

# BUILD
WORKDIR /audiveris
RUN git checkout 5.2.5
# RUN apk update && apk add tesseract-ocr tesseract-ocr-data-ita
RUN apt-get update && apt-get install -y tesseract-ocr tesseract-ocr-ita
RUN gradle wrapper --stacktrace
RUN ./gradlew clean build
RUN tar -xf build/distributions/Audiveris-5.3-alpha.tar

# CHECK WORKING
ADD test_partiture.png /audiveris/data
RUN Audiveris-5.3-alpha/bin/Audiveris -batch -export -output data/ data/test_partiture.png
RUN cd data/test_partiture && unzip test_partiture.omr

# other requirements
RUN apt-get update && apt-get install -y lilypond libsndfile-dev

WORKDIR /data

CMD "/audiveris/Audiveris-5.3-alpha/bin/Audiveris"
