# audiveris-docker
Audiveris is a OCR for music partiture (OMR: Optical Music Recognition), that software is java based and it does not have an official docker container, I provide a docker container for it. In particular this container can be runned as command line tool or also with gui

```
docker pull registry.gitlab.com/nicolalandro/audiveris-docker/audiveris:5.2.5
# Use with command line
docker run --rm -v </path/to/folder/data>:/data registry.gitlab.com/nicolalandro/audiveris-docker/audiveris:5.2.5 /audiveris/Audiveris-5.3-alpha/bin/Audiveris -batch -export -option org.audiveris.omr.sheet.BookManager.useOpus=true -option org.audiveris.omr.sheet.BookManager.useCompression=false <IMAGE_PATH>
# Use with gui
# remember that instruction once `xhost +"local:docker@"`
docker run --rm -v </path/to/folder/data>:/data -v /tmp/.X11-unix:/tmp/.X11-unix -r DISPLAY=${DISPLAY} registry.gitlab.com/nicolalandro/audiveris-docker/audiveris:5.2.5
```

## Run dev
* without gui
```
docker-compose run audiveris-bash bash
$ /audiveris/Audiveris-5.3-alpha/bin/Audiveris -batch -export -output /data test_partiture.png
```
* with gui
```
xhost +"local:docker@"
docker-compose run audiveris-scratch /audiveris/Audiveris-5.3-alpha/bin/Audiveris 
```

# References
* [audiveris](https://github.com/Audiveris/audiveris): the software to use 
* [docker](https://www.docker.com/): to containerize the app
* [docker-compose](https://docs.docker.com/compose/install/)
* [gitlab-ci/cd](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html): create build pipeline on gitlab
* [build docker on gitlab-ci/cd](https://docs.gitlab.com/ee/user/packages/container_registry/): to build and put docker image on gitlab container registry
